#include <stdio.h>
#include <stdlib.h>
#include "Viewer.h"

Viewer PBD_viewer;

void Init()
{
	PBD_viewer.Initialize();
}

void Draw()
{
	PBD_viewer.Render();
}

void Idle()
{
	if (PBD_viewer.working == true)
		PBD_viewer.Idle();
}

void Reshape(int w, int h)
{
	PBD_viewer.Reshape(w, h);
}

void SpecialKeyboard(int key, int x, int y)
{
	PBD_viewer.SpecialKey(key, x, y);
}

void Keyboard(unsigned char key, int x, int y)
{
	PBD_viewer.Keyborad(key, x, y);
}

void Mouse(int mouse_event, int state, int x, int y)
{
	PBD_viewer.Mouse(mouse_event, state, x, y);
}

void Motion(int x, int y)
{
	PBD_viewer.Motion(x, y);
}

int main(int argc, char* argv[])
{
	Init();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowPosition(550, 100);
	glutInitWindowSize(800, 800);
	glutCreateWindow("Position Based Dynamics by MJ");
	glutDisplayFunc(Draw);
	glutReshapeFunc(Reshape);
	glutMouseFunc(Mouse);
	glutMotionFunc(Motion);
	glutSpecialFunc(SpecialKeyboard);
	glutKeyboardFunc(Keyboard);
	glutIdleFunc(Idle);
	PBD_viewer.simul.printSimulInfo();
	
	glutMainLoop();

	return 0;
}