#ifndef _Simulator_H_
#define _Simulator_H_

#pragma once
#include "PBD.h"
#include <gl/glut.h>


class Simulator
{
public:
	Simulator();
	~Simulator();

	PBD pbd;

public:
	
	bool	FIXED1 = false,
			FIXED2 = false,
			FIXED4 = false;

	int		width, height;	// vertex(or cloth) size [width x height]
	
	float	moving;

	Vector	eyes, view;

	bool	drawVertex	= true,
			drawEdge	= true,
			drawMesh	= false,
			drawNormal	= false,
			drawVelo	= false;

public:

	void initialize();
		void initVertex();
		void initEdge();
		void initMesh();
		void initAdjacentMesh();
	void update();
	void render();

	void DrawVertex();
	void DrawEdge();
	void DrawMesh();
	void DrawNormal();
	void DrawVelo();

	void move(string input);

	void printSimulInfo();
};

#endif