#pragma once
#include <gl/GL.h>
#include <gl/GLU.h>
#include <gl/GLAUX.H>
#include <gl/glut.h>
#include <iostream>
#include "Simulator.h"

class Viewer
{
public:
	Viewer();
	~Viewer();

public:

	Simulator simul;

	int mouseX, mouseY;
	unsigned char mouseEventL, mouseEventM, mouseEventR;

	float rotateX, rotateY, rotateZ;
	float transX, transY, transZ;
	float zoom;

	bool working;

public:
	void DrawAxis();

	void Initialize();
	void Render();
	void Reshape(int w, int h);
	void Keyborad(unsigned char key, int x, int y);
	void SpecialKey(int key, int x, int y);
	void Mouse(int mouse_event, int state, int x, int y);
	void Motion(int x, int y);
	void Idle();

	void printReplay();
};