#include "Simulator.h"
#include <iostream>

#define index(i,j) ((i)+(width+1)*(j))

int INDEX(int i, int j)
{
	return (i + (10 /*height*/ + 1)*(j - 1) - 1);
}

Simulator::Simulator()
{
	FIXED1 = true;

	width	= 10;
	height	= 10;

	moving = width*0.01f;

	eyes = { (float)width / 2.0f, (float)height / 2.0f, (float)(width + height) };
	view = { (float)width / 2.0f, (float)height / 2.0f, 0.0f };
}


Simulator::~Simulator()
{
	pbd.vertex.clear();
	pbd.edge.clear();
	pbd.mesh.clear();
}

void Simulator::initialize()
{
	pbd.vertex.clear();
	pbd.edge.clear();
	pbd.mesh.clear();

	initVertex();
	initEdge();
	initMesh();
	initAdjacentMesh();
}

void Simulator::initVertex()
{
	int IDX = 0; // vertex array index, vertex[IDX]

	for (float j = 0; j <= height; j++)
		for (float i = 0; i <= width; i++)
		{
			Vertex v;

			v.idx = IDX;
			v.mass = 1.0f;
			v.w = 1.0f / v.mass;
			if (FIXED1)
			{
				v.position = { i, pbd.bottom, j };

				if ((i == 0 && j == (height / 2)) || (i == 0 && j == (height / 2 + 1)))
					v.fixed = true;
			}
			else if (FIXED2)
			{
				v.position = { i, j, 0.0f };
				
				if ((i == 0 && j == height) || (i == width && j == height))
					v.fixed = true;
			}
			else if (FIXED4)
			{
				v.position = { i, (float)height/2.0f, j };

				if ((i == 0 && j == height) || (i == width && j == height) || (i == width && j == 0) || (i == 0 && j == 0))
					v.fixed = true;

				eyes.y = (float)height;
			}
			v.velocity.make0();
			v.force.make0();

			pbd.vertex.push_back(v);

			IDX++;
		}
}

void Simulator::initEdge()
{
	for (int j = 0; j <= height; j++)
		for (int i = 0; i <= width; i++)
		{
			Edge e;

			e.p1 = &pbd.vertex[index(i, j)];
			
			/* structural spring */
			if (i != width)
			{
				e.p2 = &pbd.vertex[index(i + 1, j)];
				pbd.edge.push_back(e);
			}

			if (j != height)
			{
				e.p2 = &pbd.vertex[index(i, j + 1)];
				pbd.edge.push_back(e);
			}

			/* shear spring ver.2 */
			if (((i % 2 == 1 && j % 2 == 0) || (i % 2 == 0 && j % 2 == 1)) && (i != width && j != height))
			{
				e.p2 = &pbd.vertex[index(i + 1, j + 1)];
				pbd.edge.push_back(e);
			}

			if (((i % 2 == 1 && j % 2 == 1) || (i % 2 == 0 && j % 2 == 0)) && (i != width && j != height))
			{
				e.p1 = &pbd.vertex[index(i + 1, j)];
				e.p2 = &pbd.vertex[index(i, j + 1)];
				pbd.edge.push_back(e);
			}

		}

	/* compute initial length of edge */
	for (int j = 0; j < pbd.edge.size(); j++)
	{
		Vector length;

		length = pbd.edge[j].p1->position - pbd.edge[j].p2->position;

		pbd.edge[j].init_len = length.norm();
	}
}

void Simulator::initMesh()
{
	/* init mesh by vertex */
	for (int j = 0; j < height; j++)
		for (int i = 0; i < width; i++)
		{
			Mesh h;

			if ((i % 2 == 0 && j % 2 == 0) || (i % 2 == 1 && j % 2 == 1))
			{
				h.p1 = &pbd.vertex[index(i, j)];
				h.p2 = &pbd.vertex[index(i + 1, j)];
				h.p3 = &pbd.vertex[index(i, j + 1)];
				pbd.mesh.push_back(h);

				h.p1 = &pbd.vertex[index(i + 1, j)];
				h.p2 = &pbd.vertex[index(i + 1, j + 1)];
				h.p3 = &pbd.vertex[index(i, j + 1)];
				pbd.mesh.push_back(h);
			}
			else
			{
				h.p1 = &pbd.vertex[index(i, j)];
				h.p2 = &pbd.vertex[index(i + 1, j + 1)];
				h.p3 = &pbd.vertex[index(i, j + 1)];
				pbd.mesh.push_back(h);

				h.p1 = &pbd.vertex[index(i, j)];
				h.p2 = &pbd.vertex[index(i + 1, j)];
				h.p3 = &pbd.vertex[index(i + 1, j + 1)];
				pbd.mesh.push_back(h);
			}
		}

	/* init edges of mesh */
	for (int k = 0; k < pbd.mesh.size(); k++)
		for (int j = 0; j < pbd.edge.size(); j++)
		{
			if (((pbd.edge[j].p1 == pbd.mesh[k].p1) && (pbd.edge[j].p2 == pbd.mesh[k].p2))
				|| ((pbd.edge[j].p1 == pbd.mesh[k].p2) && (pbd.edge[j].p2 == pbd.mesh[k].p1)))
				pbd.mesh[k].e1 = &pbd.edge[j];

			if (((pbd.edge[j].p1 == pbd.mesh[k].p2) && (pbd.edge[j].p2 == pbd.mesh[k].p3))
				|| ((pbd.edge[j].p1 == pbd.mesh[k].p3) && (pbd.edge[j].p2 == pbd.mesh[k].p2)))
				pbd.mesh[k].e2 = &pbd.edge[j];
			
			if (((pbd.edge[j].p1 == pbd.mesh[k].p1) && (pbd.edge[j].p2 == pbd.mesh[k].p3))
				|| ((pbd.edge[j].p1 == pbd.mesh[k].p3) && (pbd.edge[j].p2 == pbd.mesh[k].p1)))
				pbd.mesh[k].e3 = &pbd.edge[j];
		}

	/* init normal vector */
	Vector p1p2, p1p3, n;
	for (int k = 0; k < pbd.mesh.size(); k++)
	{
		p1p2 = pbd.mesh[k].p2->position - pbd.mesh[k].p1->position;
		p1p3 = pbd.mesh[k].p3->position - pbd.mesh[k].p1->position;
		n = p1p2.cross(p1p3);
		n.normalize();

		pbd.mesh[k].normal = n;
	}
}

void Simulator::initAdjacentMesh()
{
	for (int j = 0; j < pbd.edge.size(); j++)
		for (int k = 0; k < pbd.mesh.size(); k++)
		{
			if (&pbd.edge[j] == pbd.mesh[k].e1)
			{
				if (pbd.edge[j].m1 == NULL)
					pbd.edge[j].m1 = &pbd.mesh[k];
				else
					pbd.edge[j].m2 = &pbd.mesh[k];
			}
			else if (&pbd.edge[j] == pbd.mesh[k].e2)
			{
				if (pbd.edge[j].m1 == NULL)
					pbd.edge[j].m1 = &pbd.mesh[k];
				else
					pbd.edge[j].m2 = &pbd.mesh[k];
			}
			else if (&pbd.edge[j] == pbd.mesh[k].e3)
			{
				if (pbd.edge[j].m1 == NULL)
					pbd.edge[j].m1 = &pbd.mesh[k];
				else
					pbd.edge[j].m2 = &pbd.mesh[k];
			}
		}
}

void Simulator::update()
{
	pbd.Velocity_step();
	pbd.Damping();
	pbd.Position_step();
	pbd.Projection();
	pbd.DataModifying();
	pbd.Velocity_update();
	pbd.CloseBottom();
	pbd.UpdateNormalOfMesh();
}

void Simulator::render()
{
	if (drawVertex) DrawVertex();
	if (drawEdge) DrawEdge();
	if (drawMesh) DrawMesh();
	if (drawNormal) DrawNormal();
	if (drawVelo) DrawVelo();
}

void Simulator::DrawVertex()
{
	glPointSize(5.0f);

	/* Draw fixed points only */
	glBegin(GL_POINTS);
	for (int i = 0; i < pbd.vertex.size(); i++)
		if (pbd.vertex[i].fixed == true)
		{
			glColor3f(1.0f, 0.0f, 0.0f);
			glVertex3f(pbd.vertex[i].position.x, pbd.vertex[i].position.y, pbd.vertex[i].position.z);
		}
	glEnd();
}

void Simulator::DrawEdge()
{
	/* Draw using edge */
	glColor3f(0.5f, 0.5f, 0.5f);

	glBegin(GL_LINES);
	for (int j = 0; j < pbd.edge.size(); j++)
	{
		glVertex3f(pbd.edge[j].p1->position.x, pbd.edge[j].p1->position.y, pbd.edge[j].p1->position.z);
		glVertex3f(pbd.edge[j].p2->position.x, pbd.edge[j].p2->position.y, pbd.edge[j].p2->position.z);
	}
	glEnd();


	/* Check the adjacent meshes of edge*/
	//int input = 1; // 확인할 edge number
	//
	//glBegin(GL_LINES);
	//for (int j = 0; j < pbd.edge.size(); j++)
	//{
	//	if (j == input) glColor3f(1.0f, 1.0f, 1.0f); // 확인할 edge이면 하얀색
	//	else 	glColor3f(0.5f, 0.5f, 0.5f); // 아니면 회색
	//
	//	glVertex3f(pbd.edge[j].p2->position.x, pbd.edge[j].p2->position.y, pbd.edge[j].p2->position.z);
	//	glVertex3f(pbd.edge[j].p1->position.x, pbd.edge[j].p1->position.y, pbd.edge[j].p1->position.z);
	//}
	//glEnd();
	//
	//
	////확인할 edge의 adjacent mesh 확인
	//glColor3f(1.0f, 0.0f, 0.0f);
	//
	//glBegin(GL_TRIANGLES);
	//glVertex3f(pbd.edge[input].m1->p1->position.x, pbd.edge[input].m1->p1->position.y, pbd.edge[input].m1->p1->position.z);
	//glVertex3f(pbd.edge[input].m1->p2->position.x, pbd.edge[input].m1->p2->position.y, pbd.edge[input].m1->p2->position.z);
	//glVertex3f(pbd.edge[input].m1->p3->position.x, pbd.edge[input].m1->p3->position.y, pbd.edge[input].m1->p3->position.z);
	//if (pbd.edge[input].m2 != nullptr) // 두 번 째 mesh가 비어있다면 그리지 않음
	//{
	//	glVertex3f(pbd.edge[input].m2->p1->position.x, pbd.edge[input].m2->p1->position.y, pbd.edge[input].m2->p1->position.z);
	//	glVertex3f(pbd.edge[input].m2->p2->position.x, pbd.edge[input].m2->p2->position.y, pbd.edge[input].m2->p2->position.z);
	//	glVertex3f(pbd.edge[input].m2->p3->position.x, pbd.edge[input].m2->p3->position.y, pbd.edge[input].m2->p3->position.z);
	//}
	//glEnd();


	/* Draw using meshes*/
	//glBegin(GL_LINES);
	//for (int k = 0; k < pbd.mesh.size(); k++)
	//{
	//	glVertex3f(pbd.mesh[k].p1->position.x, pbd.mesh[k].p1->position.y, pbd.mesh[k].p1->position.z);
	//	glVertex3f(pbd.mesh[k].p2->position.x, pbd.mesh[k].p2->position.y, pbd.mesh[k].p2->position.z);
	//
	//	glVertex3f(pbd.mesh[k].p2->position.x, pbd.mesh[k].p2->position.y, pbd.mesh[k].p2->position.z);
	//	glVertex3f(pbd.mesh[k].p3->position.x, pbd.mesh[k].p3->position.y, pbd.mesh[k].p3->position.z);
	//
	//	glVertex3f(pbd.mesh[k].p3->position.x, pbd.mesh[k].p3->position.y, pbd.mesh[k].p3->position.z);
	//	glVertex3f(pbd.mesh[k].p1->position.x, pbd.mesh[k].p1->position.y, pbd.mesh[k].p1->position.z);
	//
	//	// mesh에 edge 정보를 넣고 edge로 그려야된다. vertex 기준으로 그리게되면 겹치는 edge들이 많음. 이라고 했는데 어떻게 그리던 똑같네ㅇㅅㅇ
	//}
	//glEnd();


	/* Draw using edges of mesh*/
	//glBegin(GL_LINES);
	//for (int k = 0; k < pbd.mesh.size(); k++)
	//{
	//	glVertex3f(pbd.mesh[k].e1->p1->position.x, pbd.mesh[k].e1->p1->position.y, pbd.mesh[k].e1->p1->position.z);
	//	glVertex3f(pbd.mesh[k].e1->p2->position.x, pbd.mesh[k].e1->p2->position.y, pbd.mesh[k].e1->p2->position.z);
	//
	//	glVertex3f(pbd.mesh[k].e2->p1->position.x, pbd.mesh[k].e2->p1->position.y, pbd.mesh[k].e2->p1->position.z);
	//	glVertex3f(pbd.mesh[k].e2->p2->position.x, pbd.mesh[k].e2->p2->position.y, pbd.mesh[k].e2->p2->position.z);
	//
	//	glVertex3f(pbd.mesh[k].e3->p1->position.x, pbd.mesh[k].e3->p1->position.y, pbd.mesh[k].e3->p1->position.z);
	//	glVertex3f(pbd.mesh[k].e3->p2->position.x, pbd.mesh[k].e3->p2->position.y, pbd.mesh[k].e3->p2->position.z);
	//}
	//glEnd();


	/* Draw using adjacent mesh of edge */
	//glBegin(GL_LINES);
	//for (int j = 0; j < pbd.edge.size(); j++)
	//{
	//	glVertex3f(pbd.edge[j].m1->e1->p1->position.x, pbd.edge[j].m1->e1->p1->position.y, pbd.edge[j].m1->e1->p1->position.z);
	//	glVertex3f(pbd.edge[j].m1->e1->p2->position.x, pbd.edge[j].m1->e1->p2->position.y, pbd.edge[j].m1->e1->p2->position.z);
	//
	//	glVertex3f(pbd.edge[j].m1->e2->p1->position.x, pbd.edge[j].m1->e2->p1->position.y, pbd.edge[j].m1->e2->p1->position.z);
	//	glVertex3f(pbd.edge[j].m1->e2->p2->position.x, pbd.edge[j].m1->e2->p2->position.y, pbd.edge[j].m1->e2->p2->position.z);
	//
	//	glVertex3f(pbd.edge[j].m1->e3->p1->position.x, pbd.edge[j].m1->e3->p1->position.y, pbd.edge[j].m1->e3->p1->position.z);
	//	glVertex3f(pbd.edge[j].m1->e3->p2->position.x, pbd.edge[j].m1->e3->p2->position.y, pbd.edge[j].m1->e3->p2->position.z);
	//
	//	if (pbd.edge[j].m2 != NULL)
	//	{
	//		glVertex3f(pbd.edge[j].m2->e1->p1->position.x, pbd.edge[j].m2->e1->p1->position.y, pbd.edge[j].m2->e1->p1->position.z);
	//		glVertex3f(pbd.edge[j].m2->e1->p2->position.x, pbd.edge[j].m2->e1->p2->position.y, pbd.edge[j].m2->e1->p2->position.z);
	//
	//		glVertex3f(pbd.edge[j].m2->e2->p1->position.x, pbd.edge[j].m2->e2->p1->position.y, pbd.edge[j].m2->e2->p1->position.z);
	//		glVertex3f(pbd.edge[j].m2->e2->p2->position.x, pbd.edge[j].m2->e2->p2->position.y, pbd.edge[j].m2->e2->p2->position.z);
	//
	//		glVertex3f(pbd.edge[j].m2->e3->p1->position.x, pbd.edge[j].m2->e3->p1->position.y, pbd.edge[j].m2->e3->p1->position.z);
	//		glVertex3f(pbd.edge[j].m2->e3->p2->position.x, pbd.edge[j].m2->e3->p2->position.y, pbd.edge[j].m2->e3->p2->position.z);
	//	}
	//
	//}
	//glEnd();
}

void Simulator::DrawMesh()
{
	glColor3f(1.0f, 1.0f, 1.0f);

	glBegin(GL_TRIANGLES);
	for (int k = 0; k < pbd.mesh.size(); k++)
	{
		glVertex3f(pbd.mesh[k].p1->position.x, pbd.mesh[k].p1->position.y, pbd.mesh[k].p1->position.z);
		glVertex3f(pbd.mesh[k].p2->position.x, pbd.mesh[k].p2->position.y, pbd.mesh[k].p2->position.z);
		glVertex3f(pbd.mesh[k].p3->position.x, pbd.mesh[k].p3->position.y, pbd.mesh[k].p3->position.z);
	}
	glEnd();
}

void Simulator::DrawNormal()
{
	glColor3f(0.5f, 1.0f, 0.5f);

	glBegin(GL_LINES);
	for (int k = 0; k < pbd.mesh.size(); k++)
	{
		Vector cm; cm.equal(0.0f);
		Vector nm; nm.equal(0.0f);

		cm = (pbd.mesh[k].p1->position + pbd.mesh[k].p2->position + pbd.mesh[k].p3->position) / 3.0f;
		nm = cm + pbd.mesh[k].normal/2.0f;

		glVertex3f(cm.x, cm.y, cm.z);
		glVertex3f(nm.x, nm.y, nm.z);
	}
	glEnd();
}

void Simulator::DrawVelo()
{
	glColor3f(1.0f, 0.5f, 0.5f);

	glBegin(GL_LINES);
	for (int i = 0; i < pbd.vertex.size(); i++)
	{
		Vector dir = pbd.vertex[i].velocity; dir.normalize();
		dir = dir / 5.0f;

		glVertex3f(pbd.vertex[i].position.x, pbd.vertex[i].position.y, pbd.vertex[i].position.z);
		glVertex3f(pbd.vertex[i].position.x + dir.x, pbd.vertex[i].position.y + dir.y, pbd.vertex[i].position.z + dir.z);
	}
	glEnd();
}

void Simulator::move(string input)
{
	if (input == "left")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed==true)
				pbd.vertex[i].position.x -= moving;
		}
	}

	if (input == "right")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed == true)
				pbd.vertex[i].position.x += moving;
		}
	}

	if (input == "up")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed == true)
				pbd.vertex[i].position.y += moving;
		}
	}

	if (input == "down")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed == true)
				pbd.vertex[i].position.y -= moving;
		}
	}

	if (input == "front")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed == true)
				pbd.vertex[i].position.z += moving;
		}
	}

	if (input == "back")
	{
		for (int i = 0; i < pbd.vertex.size(); i++)
		{
			if (pbd.vertex[i].fixed == true)
				pbd.vertex[i].position.z -= moving;
		}
	}

	if (input == "recede")
	{
		
	}

	if (input == "approach")
	{
		
	}
}

void Simulator::printSimulInfo()
{
	cout << pbd.vertex.size() << " vertics, " << pbd.edge.size() << " edges, " << pbd.mesh.size() << " meshes" << endl << endl;

	cout << "---------------[ KEY ]-------------" << endl
		<< endl
		<< "	MOVING" << endl<<endl
		<< " → 		Right" << endl
		<< " ←		Left" << endl
		<< " ↑		Up" << endl
		<< " ↓		Down" << endl
		<< " Z		Front" << endl
		<< " X		Back" << endl
		<< endl << endl
		<< "	DRAW" << endl << endl
		<< " P		Draw vertex" << endl
		<< " E		Draw edge" << endl
		<< " M		Draw mesh" << endl
		<< " V		Draw velocity" << endl
		<< " N		Draw Face normal" << endl
		<< endl << endl
		<< "	SIMULATION" << endl << endl
		<< " R		Replay" << endl
		<< " SPACE BAR	Start/Pause" << endl
		<< " Q or ESC	Exit" << endl
		<< endl
		<< "-----------------------------------" << endl;
}