#ifndef _Mesh_H_
#define _Mesh_H_

#pragma once
#include "Edge.h"
#include <vector>

class Edge;

class Mesh
{
public:
	Mesh();
	~Mesh();

	Vertex	*p1, *p2, *p3;
	Edge	*e1, *e2, *e3;
	Vector	normal;
};

#endif