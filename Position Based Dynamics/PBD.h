#ifndef _PBD_H_
#define _PBD_H_

#pragma once
#include <vector>
#include <iostream>
#include "Edge.h"
#include "Mesh.h"

using namespace std;

class PBD
{
public:
	PBD();
	~PBD();

	vector<Vertex> vertex;
	vector<Edge> edge;
	vector<Mesh> mesh;

	int	Ns;		// iteration

	float k_stretch, k_bend, k_coll;	// stiffness; strength of the constraint, [0,1]
	float dihedral_angle;
	float thickness;
	float dt;
	float bottom;
	Vector gravity = { 0.0f, -9.8f, 0.0f };

	Vector ExternalForce(Vertex v);
		Vector Gravity(Vertex v);
	void Velocity_step();
	void Damping();
	void Position_step();
	void Constraint_DistanceProjection();
	void Constraint_BendingProjection();
	void Constraint_SelfCollisionProjection();
	void Projection();
	void DataModifying();
	void Velocity_update();

	void CloseBottom();
	void UpdateNormalOfMesh();
};

#endif