#include "PBD.h"

PBD::PBD()
{
	Ns = 1;

	k_stretch = 1.0f;
	k_bend = 0.5f;
	k_coll = 1.0f;

	dihedral_angle = 3.14159265359f;
	thickness = 1.0f;
	dt = 0.01f;
	bottom = -1.0f;
}


PBD::~PBD()
{
}

Vector PBD::ExternalForce(Vertex v)
{
	return Gravity(v);
}

Vector PBD::Gravity(Vertex v)
{
	Vector f = { 0.0f, 0.0f, 0.0f };

	f = v.mass*gravity;

	return f;
}

void PBD::Velocity_step()
{
	for (int i = 0; i < vertex.size(); i++)
	{
		vertex[i].velocity += dt*vertex[i].w*ExternalForce(vertex[i]);
	}
}


void PBD::Damping()
{
	/* simplified version */
	for (int i = 0; i < vertex.size(); i++)
	{
		vertex[i].velocity = 0.99*vertex[i].velocity;
	}
}

void PBD::Position_step()
{
	for (int i = 0; i < vertex.size(); i++)
	{
		vertex[i].position_correction = vertex[i].position + dt*vertex[i].velocity;
	}
}

void PBD::Constraint_DistanceProjection()
{
	/* equality */

	for (int j = 0; j < edge.size(); j++)
	{
		Vector p1p2 = edge[j].p2->position_correction - edge[j].p1->position_correction;
		float constraint = (edge[j].p2->position_correction - edge[j].p1->position_correction).norm() - edge[j].init_len;
		float norm = p1p2.norm();
		p1p2.normalize();

		Vector dp1 = (+0.5f*constraint*p1p2 / norm)*(1.0f - pow(1.0f - k_stretch, Ns));
		Vector dp2 = (-0.5f*constraint*p1p2 / norm)*(1.0f - pow(1.0f - k_stretch, Ns));

		if (constraint != 0)
		{
			if (edge[j].p1->fixed == true)
			{
				edge[j].p2->position_correction += 2.0f*dp2;
			}
			else if (edge[j].p2->fixed == true)
			{
				edge[j].p1->position_correction += 2.0f*dp1;
			}
			else
			{
				edge[j].p1->position_correction += dp1;
				edge[j].p2->position_correction += dp2;
			}
		}
	}
}

void PBD::Constraint_BendingProjection()
{
	/* equality */

	for (int j = 0; j < edge.size(); j++)
	{
		if (edge[j].m2 == NULL) continue;

		Vertex *v1 = nullptr, *v2 = nullptr, *v3 = nullptr, *v4 = nullptr;

		v1 = edge[j].p1;
		v2 = edge[j].p2;

		if		((edge[j].m1->p1 != edge[j].p1) && (edge[j].m1->p1 != edge[j].p2)) v3 = edge[j].m1->p1;
		else if ((edge[j].m1->p2 != edge[j].p1) && (edge[j].m1->p2 != edge[j].p2)) v3 = edge[j].m1->p2;
		else if ((edge[j].m1->p3 != edge[j].p1) && (edge[j].m1->p3 != edge[j].p2)) v3 = edge[j].m1->p3;

		if		((edge[j].m2->p1 != edge[j].p1) && (edge[j].m2->p1 != edge[j].p2)) v4 = edge[j].m2->p1;
		else if ((edge[j].m2->p2 != edge[j].p1) && (edge[j].m2->p2 != edge[j].p2)) v4 = edge[j].m2->p2;
		else if ((edge[j].m2->p3 != edge[j].p1) && (edge[j].m2->p3 != edge[j].p2)) v4 = edge[j].m2->p3;


		Vector n1, n2;

		n1 = edge[j].m1->normal;
		n2 = -1.0f*edge[j].m2->normal;

		float d = n1*n2;
		float constraint = acos(d) - dihedral_angle;


		Vector q1, q2, q3, q4;
		q3 = (v2->position_correction.cross(n2) + (n1.cross(v2->position_correction))*d) / (v2->position_correction.cross(v3->position_correction)).norm();
		q4 = (v2->position_correction.cross(n1) + (n2.cross(v2->position_correction))*d) / (v2->position_correction.cross(v4->position_correction)).norm();
		q2 = ((v3->position_correction.cross(n2) + (n1.cross(v3->position_correction))*d) / (v2->position_correction.cross(v3->position_correction)).norm()
			+ (v4->position_correction.cross(n1) + (n2.cross(v4->position_correction))*d) / (v2->position_correction.cross(v4->position_correction)).norm())*-1.0f;
		q1 = (q2 + q3 + q4)*-1.0f;


		Vector dp1, dp2, dp3, dp4;
		dp1 = -1.0f*(v1->w*sqrt(1.0f - d*d)*constraint / (v2->w*q2.norm()*q2.norm() + v3->w*q3.norm()*q3.norm() + v4->w*q4.norm()*q4.norm()))*q1*(1.0f - pow(1.0f - k_bend, Ns));
		dp2 = -1.0f*(v2->w*sqrt(1.0f - d*d)*constraint / (v1->w*q1.norm()*q1.norm() + v3->w*q3.norm()*q3.norm() + v4->w*q4.norm()*q4.norm()))*q2*(1.0f - pow(1.0f - k_bend, Ns));
		dp3 = -1.0f*(v3->w*sqrt(1.0f - d*d)*constraint / (v1->w*q1.norm()*q1.norm() + v2->w*q2.norm()*q2.norm() + v4->w*q4.norm()*q4.norm()))*q3*(1.0f - pow(1.0f - k_bend, Ns));
		dp4 = -1.0f*(v4->w*sqrt(1.0f - d*d)*constraint / (v1->w*q1.norm()*q1.norm() + v2->w*q2.norm()*q2.norm() + v3->w*q3.norm()*q3.norm()))*q4*(1.0f - pow(1.0f - k_bend, Ns));
		

		if (constraint != 0)
		{
			v1->position_correction += dp1;
			v2->position_correction += dp2;
			v3->position_correction += dp3;
			v4->position_correction += dp4;
		}
	}
}

void PBD::Constraint_SelfCollisionProjection()
{
	/* inequality */

	for (int k = 0; k < mesh.size(); k++)
		for (int i = 0; i < vertex.size(); i++)
		{
			
		}
}

void PBD::Projection()
{
	for (int iter = 0; iter < 50; iter++)
	{
		Constraint_DistanceProjection();
		Constraint_SelfCollisionProjection();

		Ns++;
	}

	if (Ns > 50) Ns = 1;
}

void PBD::DataModifying()
{
	for (int i = 0; i < vertex.size(); i++)
	{
		vertex[i].velocity = (vertex[i].position_correction - vertex[i].position) / dt;

		if (vertex[i].fixed == true)
			vertex[i].position = vertex[i].position;
		else
			vertex[i].position = vertex[i].position_correction;
	}
}

void PBD::Velocity_update()
{

}

void PBD::CloseBottom()
{
	for (int i = 0; i < vertex.size(); i++)
		if (vertex[i].position.y < bottom)
			vertex[i].position.y = bottom;
}

void PBD::UpdateNormalOfMesh()
{
	Vector p1p2, p1p3, n;

	for (int k = 0; k < mesh.size(); k++)
	{
		p1p2 = mesh[k].p2->position - mesh[k].p1->position;
		p1p3 = mesh[k].p3->position - mesh[k].p1->position;

		n = p1p2.cross(p1p3);
		n.normalize();

		mesh[k].normal = n;
	}
}