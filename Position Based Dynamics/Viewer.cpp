#include "Viewer.h"


Viewer::Viewer()
{
	mouseX = mouseY = 0;
	mouseEventL = mouseEventM = mouseEventR = 0;

	rotateX = rotateY = rotateZ = 0;
	transX = transY = transZ = 0;
	zoom = 0;

	working = false;
}


Viewer::~Viewer()
{
}

void Viewer::DrawAxis()
{
	glBegin(GL_LINES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, 1.0f);

	glEnd();
}

void Viewer::Initialize()
{
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	simul.initialize();
}

void Viewer::Idle()
{
	simul.update();
}

void Viewer::Render()
{
	glEnable(GL_DEPTH_TEST);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	
	gluLookAt(simul.eyes.x, simul.eyes.y, simul.eyes.z, simul.view.x, simul.view.y, simul.view.z, 0.0f, 1.0f, 0.0f);

	DrawAxis();

	glTranslatef((float)simul.width / 2, (float)simul.height / 2, 0.0f);
	glTranslatef(0.0f, 0.0f, zoom);
	glTranslatef(transX, -transY, 0.0f);
	glRotatef(rotateX, 0.0f, 1.0f, 0.0f);
	glRotatef(rotateY, 1.0f, 0.0f, 0.0f);
	glTranslatef((float)-simul.width / 2, (float)-simul.height / 2, 0.0f);
	
	simul.render();

	glutSwapBuffers();
	glutPostRedisplay();
}

void Viewer::Reshape(int w, int h)
{
	glViewport(0, 0, w, h);
	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();
	gluPerspective(60.0, w / h, 1.0, 300.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Viewer::SpecialKey(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
		simul.move("up");
		break;
	case GLUT_KEY_DOWN:
		simul.move("down");
		break;
	case GLUT_KEY_LEFT:
		simul.move("left");
		break;
	case GLUT_KEY_RIGHT:
		simul.move("right");
		break;
	}
}

void Viewer::Keyborad(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'q':
	case 'Q':
	case 27:
		exit(0);
		break;
	case 'r':
		printReplay();
		Initialize();
		break;
	case 32:
		working = !working;
		break;
	case 'z':
		simul.move("front");
		break;
	case 'x':
		simul.move("back");
		break;
	case 'v':
		simul.drawVelo = !simul.drawVelo;
		break;
	case 'e':
		simul.drawEdge = !simul.drawEdge;
		break;
	case 'm':
		simul.drawMesh = !simul.drawMesh;
		break;
	case 'n':
		simul.drawNormal = !simul.drawNormal;
		break;
	case 'p':
		simul.drawVertex = !simul.drawVertex;
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Viewer::Mouse(int mouse_event, int state, int x, int y)
{
	mouseX = x;
	mouseY = y;

	switch (mouse_event)
	{
	case GLUT_LEFT_BUTTON:
		mouseEventL = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_MIDDLE_BUTTON:
		mouseEventM = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	case GLUT_RIGHT_BUTTON:
		mouseEventR = ((GLUT_DOWN == state) ? 1 : 0);
		break;
	default:
		break;
	}

	glutPostRedisplay();
}

void Viewer::Motion(int x, int y)
{
	int diffX = x - mouseX;
	int diffY = y - mouseY;

	mouseX = x;
	mouseY = y;

	if (mouseEventL)
	{
		rotateX += (float) 0.1f*diffX;
		rotateY += (float) 0.1f*diffY;
	}
	else if (mouseEventM)
	{
		zoom += (float) 0.1f*diffY;
	}
	else if (mouseEventR)
	{
		transX += (float) 0.01f*diffX;
		transY += (float) 0.01f*diffY;
	}
}

void Viewer::printReplay()
{
	static int replayterm = 1;

	cout << "Replay #" << replayterm << endl;

	replayterm++;

	/* k_stretch에 따라서 달라지는거 확인해보고싶어서 해봄 */
	//cout << "Input k_stetch (0,1] : ";
	//cin >> simul.pbd.k_stretch;
}