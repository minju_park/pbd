#ifndef _Vertex_H_
#define _Vertex_H_

#pragma once
#include "Vector.h"
#include <vector>

class Vertex
{
public:
	Vertex();
	~Vertex();

	bool	fixed = false;

	int		idx;	// index
	float	mass;
	float	w;	// inverse of mass, 1/mass
	Vector	position_correction;
	Vector	position;
	Vector	velocity;
	Vector	force;

public:
	bool operator==(Vertex v)
	{
		if (position.x == v.position.x && position.y == v.position.y && position.z == v.position.z)
			return true;
		else
			return false;
	}

	bool operator!=(Vertex v)
	{
		if (position.x != v.position.x && position.y != v.position.y && position.z != v.position.z)
			return true;
		else
			return false;
	}
};

#endif