#ifndef _Edge_H_
#define _Edge_H_

#pragma once
#include "Vertex.h"
#include <vector>

class Mesh;

class Edge
{
public:
	Edge();
	~Edge();

	Vertex	*p1, *p2;
	Mesh	*m1, *m2;

	float init_len;	// initial length
};

#endif